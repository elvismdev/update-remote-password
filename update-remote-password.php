<?php
/*
Plugin Name: Update Remote Password
Version: 1.2.0
Description: Updates the password on remote site when user changes his password on this one.
Author: Elvis Morales
Author URI: https://twitter.com/n3rdh4ck3r
Plugin URI: https://bitbucket.org/grantcardone/update-remote-password
Text Domain: update-remote-password
*/
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

define( 'URP_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
// API Definition
define( 'USER_UPDATE_PASSWORD', 'user/update_password' );

require_once( URP_PLUGIN_PATH . 'inc/urp_options.php' );

if ( is_admin() )
	new URPOptions();

//Action when reset password is gonna be done
add_action( 'password_reset', 'urp_password_reset', 10, 2 );
function urp_password_reset( $user, $new_pass ) {

	$urp_options = get_option( 'update-remote-password' ); // Array of All Options

	$params = array(
		'user_login' => urlencode( $user->user_login ),
		'user_pass' => urlencode( $new_pass )
		);

	$api_url = $urp_options['remote_site'];
	if ( substr( $api_url, -1 ) != '/' )
		$api_url .= '/';

	$api_url .= $urp_options['api_base'];
	if ( substr( $api_url, -1 ) != '/' )
		$api_url .= '/';

    // Generate the URL to update user password
	$url = $api_url . USER_UPDATE_PASSWORD;
	$url = add_query_arg( $params, esc_url_raw( $url ) );

	if ( $urp_options['debug'] == 'debug' ) {
		error_log("URL: ". $url);
	}

	$update_password_response = wp_remote_get( esc_url_raw( $url ) );

	$decoded_response = json_decode( wp_remote_retrieve_body( $update_password_response ) );

	if ( $urp_options['debug'] == 'debug' ) {
		if ( is_wp_error( $update_password_response ) )
			error_log( "WP ERROR NOTICE: ". $update_password_response->get_error_message() );
		elseif ( $decoded_response->status == 'error' )
			error_log( "ERROR NOTICE: ". $decoded_response->error );
		else
			error_log( "OK NOTICE: ". $decoded_response->message );
	}

}
