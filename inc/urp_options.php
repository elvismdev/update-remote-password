<?php 

class URPOptions {
	private $urp_options;

	public function __construct() {
		add_action( 'admin_menu', array( $this, 'urp_add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'urp_page_init' ) );
	}

	public function urp_add_plugin_page() {
		add_options_page(
			'Update Remote Password', // page_title
			'Update Remote Password', // menu_title
			'manage_options', // capability
			'update-remote-password', // menu_slug
			array( $this, 'urp_create_admin_page' ) // function
		);
	}

	public function urp_create_admin_page() {
		$this->urp_options = get_option( 'update-remote-password' ); ?>

		<div class="wrap">
			<h2>Update Remote Password</h2>
			<p></p>
			<?php //settings_errors(); ?>

			<form method="post" action="options.php">
				<?php
					settings_fields( 'urp_options' );
					do_settings_sections( 'urp_admin' );
					submit_button();
				?>
			</form>
		</div>
	<?php }

	public function urp_page_init() {
		register_setting(
			'urp_options', // option_group
			'update-remote-password', // option_name
			array( $this, 'urp_sanitize' ) // sanitize_callback
		);

		add_settings_section(
			'urp_setting_section', // id
			'Settings', // title
			array( $this, 'urp_section_info' ), // callback
			'urp_admin' // page
		);

		add_settings_field(
			'remote_site', // id
			'Remote Site', // title
			array( $this, 'urp_remote_site_callback' ), // callback
			'urp_admin', // page
			'urp_setting_section' // section
		);

		add_settings_field(
			'api_base', // id
			'API Base', // title
			array( $this, 'urp_api_base_callback' ), // callback
			'urp_admin', // page
			'urp_setting_section' // section
		);

		add_settings_field(
			'debug', // id
			'Debug', // title
			array( $this, 'urp_debug_callback' ), // callback
			'urp_admin', // page
			'urp_setting_section' // section
		);
	}

	public function urp_sanitize($input) {
		$sanitary_values = array();
		if ( isset( $input['remote_site'] ) ) {
			$sanitary_values['remote_site'] = sanitize_text_field( $input['remote_site'] );
		}

		if ( isset( $input['api_base'] ) ) {
			$sanitary_values['api_base'] = sanitize_text_field( $input['api_base'] );
		}

		if ( isset( $input['debug'] ) ) {
			$sanitary_values['debug'] = $input['debug'];
		}

		return $sanitary_values;
	}

	public function urp_section_info() {
		
	}

	public function urp_remote_site_callback() {
		printf(
			'<input class="regular-text" type="text" name="update-remote-password[remote_site]" id="remote_site" value="%s">',
			isset( $this->urp_options['remote_site'] ) ? esc_attr( $this->urp_options['remote_site']) : ''
		);
	}

	public function urp_api_base_callback() {
		printf(
			'<input class="regular-text" type="text" name="update-remote-password[api_base]" id="api_base" value="%s">',
			isset( $this->urp_options['api_base'] ) ? esc_attr( $this->urp_options['api_base']) : ''
		);
	}

	public function urp_debug_callback() {
		printf(
			'<input type="checkbox" name="update-remote-password[debug]" id="debug" value="debug" %s> <label for="debug">Debug stuff to error_log()</label>',
			( isset( $this->urp_options['debug'] ) && $this->urp_options['debug'] === 'debug' ) ? 'checked' : ''
		);
	}

}